﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;


namespace VkMusicSoft
{
    public partial class authForm : Form
    {
        public authForm()
        {
            InitializeComponent();
        }
        [DllImport("urlmon.dll", CharSet = CharSet.Ansi)]
        private static extern int UrlMkSetSessionOption(int dwOption, string pBuffer, int dwBufferLength, int dwReserved);
        const int URLMON_OPTION_USERAGENT = 0x10000001;


        public void ChangeUserAgent(string Agent)
        {
            UrlMkSetSessionOption(URLMON_OPTION_USERAGENT, Agent, Agent.Length, 0);
        }

        public static string ParseFromURL(string Url, string index)
        {
            Url = Url.Substring(Url.IndexOf(index));
            Url = Url.Replace(index, "");
            return Url;
        }

        private void authForm_Load(object sender, EventArgs e)
        {
            ToolTip toolTipDoubliClickWebBrowser = new ToolTip();
            toolTipDoubliClickWebBrowser.SetToolTip(webBrowser1, "Для обновления страницы, два раза кликните мышью по периметру окна");

            ChangeUserAgent("Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36");
            try
            {
                webBrowser1.Navigate("https://oauth.vk.com/authorize?client_id=4647160&scope=8&redirect_uri=http://oauth.vk.com/blank.html&display=page&response_type=token");
            }
            catch { MessageBox.Show("Произошла ошибка проверьте ваше интернет-соединение", "", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            
        }

        private void webBrowser1_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            if (webBrowser1.Url.ToString().Contains("access_token"))
            {
                string gettkn = ParseFromURL(webBrowser1.Url.ToString(), "access_token=");
                gettkn = gettkn.Remove(gettkn.IndexOf("&"));
                AuthClass.token = gettkn;
                //////////////////////////////////////////////////////////////////////////////
                string get_user_id = ParseFromURL(webBrowser1.Url.ToString(), "user_id=");
                AuthClass.userID = get_user_id;
                Hide();
                mainForm form = new mainForm();
                form.ShowDialog();
            }
        }

        private void authForm_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                webBrowser1.Navigate("https://oauth.vk.com/authorize?client_id=4647160&scope=8&redirect_uri=http://oauth.vk.com/blank.html&display=page&response_type=token");
            }
            catch { MessageBox.Show("Произошла ошибка проверьте ваше интернет-соединение", "", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }
    }
}
