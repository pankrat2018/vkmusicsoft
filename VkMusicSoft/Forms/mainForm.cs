﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;
using System.Windows.Forms;

namespace VkMusicSoft
{
    public partial class mainForm : Form
    {
        public mainForm()
        {
            InitializeComponent();
        }

        List<string> _links = new List<string>();
        List<string> _artist = new List<string>();
        List<string> _title = new List<string>();
        int _selectMusic = -1;
        WMPLib.IWMPPlaylist _playlist;
        WMPLib.IWMPMedia _media;


        private void CheckDownloadFolder()
        {
            if (!Directory.Exists("Downloads"))
            {
                Directory.CreateDirectory("Downloads");
            }
        }
        private int ParseIndex()
        {
            int toDown = 0;
            for (int i = 0; i < _title.Count; i++)
            {
                if (_artist[i] + " — " + _title[i] == listBoxMusic.Items[listBoxMusic.SelectedIndex].ToString())
                {
                    toDown = i;
                }
            }
            return toDown;
        }

        private void PrintSongs()
        {
            for (int i = 0; i < _links.Count; i++)
            {
                listBoxMusic.Items.Add(_artist[i] + " — " + _title[i]);
            }
        }

        private void main_Load(object sender, EventArgs e)
        {
            ToolTip toolTipBtnExit = new ToolTip();
            toolTipBtnExit.SetToolTip(btnExit,"Выход из аккаунта ВК, с закрытием программы");
            VK api = new VK();
            api.GetMusicList(ref _links, ref _artist, ref _title);
            PrintSongs();



            _playlist = WindowsMediaPlayer.playlistCollection.newPlaylist("vkPlayList");

            for (int i = 0; i < _links.Count; i++)
            {
                _media = WindowsMediaPlayer.newMedia(_links[i]);
                _playlist.appendItem(_media);
            }
            WindowsMediaPlayer.currentPlaylist = _playlist;
            WindowsMediaPlayer.Ctlcontrols.stop();
            listBoxMusic.SelectedIndex = 0;
            _selectMusic = 0;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            string Path = Environment.GetFolderPath(Environment.SpecialFolder.Cookies);
            try
            {
                Directory.Delete(Path, true);
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
            catch (Exception)
            {
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
            
        }

        private void main_FormClosed(object sender, FormClosedEventArgs e)
        {
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }

        void DownloadSong(object fileName)
        {
            try
            {
                using (WebClient webClient = new WebClient())
                {
                    webClient.DownloadFile(fileName.ToString().Split('•')[0], "Downloads\\" + fileName.ToString().Split('•')[1] + ".mp3");
                }
            }
            catch { }
        }

        private void btnDowmloadAllSongs_Click(object sender, EventArgs e)
        {
            
            for(int i = 0; i < _links.Count; i++)
            {
                CheckDownloadFolder();
                ThreadPool.QueueUserWorkItem(DownloadAllSongs, i);
            }
        }

        void DownloadAllSongs(object id)
        {
            try
            {
                using (WebClient webClient = new WebClient())
                {
                    int index = int.Parse(id.ToString());
                    webClient.DownloadFile(_links[index], "Downloads\\" + _artist[index] + " — " + _title[index] + ".mp3");
                }
            }
            catch { }
        }

        private void textBoxSearch_TextChanged(object sender, EventArgs e)
        {
            listBoxMusic.Items.Clear();
            for (int i = 0; i < _links.Count; i++)
            {
                if(_artist[i].ToLower().Contains(textBoxSearch.Text.ToLower()) || _title[i].ToLower().Contains(textBoxSearch.Text.ToLower()))
                {
                    listBoxMusic.Items.Add(_artist[i] + " — " + _title[i]);
                }
            }
            if(textBoxSearch.Text == "")
            {
                PrintSongs();

                if(_selectMusic != -1)
                {
                    listBoxMusic.SelectedIndex = _selectMusic;
                }
            }
        }


        private void listBoxMusic_MouseDown_1(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                try
                {
                    int index = ParseIndex();

                    if (MessageBox.Show("Скачать " + _artist[index] + " — " + _title[index] + "?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        CheckDownloadFolder();
                        new Thread(DownloadSong).Start(_links[index] + "•" + _artist[index] + " — " + _title[index]); // точка - разделитель, чтобы была возможность разделить на названия
                    }
                }
                catch { MessageBox.Show("Произошла ошибка, возможно вы не выделили желаемую песню", "", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            }
        }

        private void listBoxMusic_MouseDoubleClick_1(object sender, MouseEventArgs e)
        {
            
            if (e.Button == MouseButtons.Left)
            {
                int index = ParseIndex();
               // WindowsMediaPlayer.URL = _links[ParseIndex()];
                WindowsMediaPlayer.Ctlcontrols.play();
                WindowsMediaPlayer.Ctlcontrols.currentItem = WindowsMediaPlayer.currentPlaylist.get_Item(index);
                _selectMusic = index;
            }
        }
        /// <summary>
        /// Перелистываем песни
       /// 0 = Undefined
        ///1 = Stopped(by User)
        ///2 = Paused
        ///3 = Playing
        ///4 = Scan Forward
        ///5 = Scan Backwards
        ///6 = Buffering
        ///7 = Waiting
        ///8 = Media Ended
        ///9 = Transitioning
        ///10 = Ready
        ///11 = Reconnecting
        ///12 = Last
        /// </summary>
        private void WindowsMediaPlayer_PlayStateChange(object sender, AxWMPLib._WMPOCXEvents_PlayStateChangeEvent e)
        {
             if (e.newState == 8)
             {
                ++listBoxMusic.SelectedIndex;
             }
        }

        private void listBoxMusic_KeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = true;
        }
    }
}
