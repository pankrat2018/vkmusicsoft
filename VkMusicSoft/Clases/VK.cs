﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml;

namespace VkMusicSoft
{
    class VK
    {
        public void GetMusicList(ref List<string> links, ref List<string> artist, ref List<string> title)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load("https://api.vk.com/method/audio.get.xml?uid=" + AuthClass.userID + "&access_token=" + AuthClass.token);
               // System.Diagnostics.Process.Start("https://api.vk.com/method/audio.get.xml?uid=" + AuthClass.userID + "&access_token=" + AuthClass.token);
                XmlNodeList urls = doc.SelectNodes("response/audio/url");
                foreach (XmlNode url in urls)
                {
                    links.Add(url.InnerText);
                }
                XmlNodeList artists = doc.SelectNodes("response/audio/artist");
                foreach (XmlNode _artist in artists)
                {
                    artist.Add(_artist.InnerText);
                }
                XmlNodeList titles = doc.SelectNodes("response/audio/title");
                foreach (XmlNode _title in titles)
                {
                    title.Add(_title.InnerText);
                }


            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
    }
}
