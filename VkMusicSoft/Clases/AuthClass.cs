﻿
namespace VkMusicSoft
{
  static class AuthClass
    {
        private static string _token = "";
        public static string token
        {
            get
            {
                return _token;
            }
            set
            {
                _token = value;
            }
        }

        private static string _userID = "";
        public static string userID
        {
            get
            {
                return _userID;
            }
            set
            {
                _userID = value;
            }
        }
    }
}
